//
//  WeatherData.swift
//  weather_swift
//
//  Created by Anton Medvediev on 25/03/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import Foundation

struct WeatherData: Codable{
    let name: String
    let main: Main
    let weather: [Weather]
}

struct Main: Codable {
    let temp: Double
    let feels_like: Double
}

struct Weather: Codable{
    let id: Int
}
